
!------------------------------------------------------------------------
!> function S(x)
!! evalutes the shifted boxcar function
!------------------------------------------------------------------------
REAL*8 FUNCTION s(x)
  REAL*8, INTENT(IN) :: x

  s=omega(x-0.5_8)

END FUNCTION s

