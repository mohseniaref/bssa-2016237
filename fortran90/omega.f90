
!------------------------------------------------------------------------
!> function Omega(x)
!! evaluates the boxcar function
!------------------------------------------------------------------------
REAL*8 FUNCTION omega(x)
  REAL*8, INTENT(IN) :: x

  omega=heaviside(x+0.5_8)-heaviside(x-0.5_8)

END FUNCTION omega

