#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Compute the displacement field due to distributed inelastic deformation using GMT
# grdmath command.
#
# Sylvain Barbot (04/16/16) - original form
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

usage(){
        echo "usage: $self -b -5/5/-5/5 -i 0.1 [-d 1] [...] file.grd"
	echo ""
        echo "input:"
        echo "         -b bds sets the bound to bds"
        echo "         -i inc sets the increment to inc"
	echo ""
        echo "options:"
        echo "         -d dep sets the depth [0]"
        echo "         -e e12/e13 sets the e12 and e31 inelastic strain components [1/0]"
	echo "         -o off sets the offset [0]"
        echo "         -p phi sets the dip angle [90]"
        echo "         -t thi sets the thickness [1]"
        echo "         -w wid sets the width [1]"
	echo ""
	echo "computes displacement field in file.grd"
	echo ""
        
        exit
}

# r2 = x2 - y2 = x2 - y2p sin(phi) - y3p cos(phi)
r2(){
	y2p=$1
	y3p=$2
	echo X $off SUB $phi SIND $y2p MUL SUB $phi COSD $y3p MUL SUB
}

# r3 = x3 - y3 = x3 + y2p cos(phi) - y3p sin(phi) - D
r3(){
	y2p=$1
	y3p=$2
	echo Y $phi COSD $y2p MUL ADD $phi SIND $y3p MUL SUB $dep SUB
}

# r2p = r2 sin(phi) - r3 cos(phi)
r2p(){
	y2p=$1
	y3p=$2
	echo $phi SIND `r2 $y2p $y3p` MUL $phi COSD `r3 $y2p $y3p` MUL SUB
}

# r3p = r2 cos(phi) + r3 sin(phi)
r3p(){
	y2p=$1
	y3p=$2
	echo $phi COSD `r2 $y2p $y3p` MUL $phi SIND `r3 $y2p $y3p` MUL ADD
}

# s3 = x3 - y2p cos(phi) + y3p sin(phi) + D
s3(){
	y2p=$1
	y3p=$2
	echo $phi SIND $y3p MUL $phi COSD $y2p MUL SUB Y ADD $dep ADD
}

# s2p = r2 sin(phi) + s3 cos(phi)
s2p(){
	y2p=$1
	y3p=$2
	echo $phi COSD `s3 $y2p $y3p` MUL $phi SIND `r2 $y2p $y3p` MUL ADD
}

# s3p = r2 cos(phi) - s3 sin(phi)
s3p(){
	y2p=$1
	y3p=$2
	echo $phi COSD `r2 $y2p $y3p` MUL $phi SIND `s3 $y2p $y3p` MUL SUB
}

# J12 =-2 r2p arctan({r3p}/{r2p}) - r3p ln(r2^2+r3^2)
J12(){
	y2p=$1
	y3p=$2
	echo `r3p $y2p $y3p` `r2p $y2p $y3p` DIV ATAN `r2p $y2p $y3p` MUL -2 MUL \
	     `r2 $y2p $y3p` 2 POW `r3 $y2p $y3p` 2 POW ADD LOG `r3p $y2p $y3p` MUL SUB
}

# K12 =-2 s2p arctan({s_3'}/{s_2'}) - s_3p ln(r2^2+s3^2)
K12(){
	y2p=$1
	y3p=$2
	echo `s3p $y2p $y3p` `s2p $y2p $y3p` DIV ATAN `s2p $y2p $y3p` MUL -2 MUL \
	     `r2 $y2p $y3p` 2 POW `s3 $y2p $y3p` 2 POW ADD LOG `s3p $y2p $y3p` MUL SUB
}

# J13 =-2 r3p arctan({r_2'}/{r_3'}) - r_2p ln(r2^2+r3^2)
J13(){
	y2p=$1
	y3p=$2
	echo `r2p $y2p $y3p` `r3p $y2p $y3p` DIV ATAN `r3p $y2p $y3p` MUL -2 MUL \
	     `r2 $y2p $y3p` 2 POW `r3 $y2p $y3p` 2 POW ADD LOG `r2p $y2p $y3p` MUL SUB
}

# K13 =-2 s3p arctan({s_2'}/{s_3'}) - s_2p ln(r2^2+s3^2)
K13(){
	y2p=$1
	y3p=$2
	echo `s2p $y2p $y3p` `s3p $y2p $y3p` DIV ATAN `s3p $y2p $y3p` MUL -2 MUL \
	     `r2 $y2p $y3p` 2 POW `s3 $y2p $y3p` 2 POW ADD LOG `s2p $y2p $y3p` MUL SUB
}

# u1=-1/(2 pi) [ sin(phi) [e12 (J12 + K12) + e13 (J13 + K13) ] 
#               +cos(phi) [e12 (J13 + K13) - e13 (J12 + K12) ] ]
u1(){
	y2p=$1
	y3p=$2
	echo `J12 $y2p $y3p` `K12 $y2p $y3p` ADD $e12 MUL `J13 $y2p $y3p` `K13 $y2p $y3p` ADD $e13 MUL ADD $phi SIND MUL \
	     `J13 $y2p $y3p` `K13 $y2p $y3p` ADD $e12 MUL `J12 $y2p $y3p` `K12 $y2p $y3p` ADD $e13 MUL SUB $phi COSD MUL ADD \
	     -2 PI MUL DIV
}

while getopts "hb:d:e:i:o:p:t:w:" flag
do
	case "$flag" in
	b) bset=1;bds=$OPTARG;;
	d) dset=1;dep=$OPTARG;;
	e) eset=1;e12=`echo $OPTARG | awk -F "/" '{print $1}'`;e13=`echo $OPTARG | awk -F "/" '{print $2}'`;;
	i) iset=1;inc=$OPTARG;;
	o) oset=1;off=$OPTARG;;
	p) pset=1;phi=$OPTARG;;
	t) tset=1;thi=$OPTARG;;
	w) wset=1;wid=$OPTARG;;
	h) hset=1;;
	esac
done
for item in $bset $dset $eset $iset $oset $pset $tset $wset; do
	shift;shift
done
for item in $hset;do
	shift;
done

#-------------------------------------------------------- 
# DEFAULTS
#-------------------------------------------------------- 

GRDFILE=$(dirname $1)/$(basename $1 .grd).grd
if [ "$GRDFILE" == "" ]; then
	echo "empty output file"
	echo ""
	usage
	exit 1
fi

if [ "$bset" != "1" ]; then
	echo "empty bounds"
	echo ""
	usage
	exit 1
fi

if [ "$dset" != "1" ]; then
	dep=0
fi

if [ "$oset" != "1" ]; then
	off=0
fi

if [ "$pset" != "1" ]; then
	phi=90
fi

if [ "$wset" != "1" ]; then
	wid=1
fi

if [ "$tset" != "1" ]; then
	thi=1
fi

if [ "$eset" != "1" ]; then
	e12=1
	e13=0
fi

T2=`echo $thi | awk '{print $1 / 2}'`

echo "# $self: D=$dep, phi=$phi, thickness=$thi, width=$wid, e12=$e12, e13=$e13"
# displacement field
grdmath -R$bds -I$inc `u1 $T2 $wid` `u1 -$T2 $wid` SUB `u1 $T2 0` SUB `u1 -$T2 0` ADD = $GRDFILE



