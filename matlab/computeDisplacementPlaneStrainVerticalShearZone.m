function [u2,u3]=computeDisplacementPlaneStrainVerticalShearZone( ...
    x2,x3,q2,q3,T,W,epsv22,epsv23,epsv33,G,nu)
% function COMPUTEDISPLACEMENTPLANESTRAINVERTICALSHEARZONE computes the
% displacement field associated with deforming vertical shear zones
% considering the following geometry using the analytical solution.
%
%                   surface
%   -------------------+------------------- E (x2)
%                      |
%                      |
%                      |
%                      |     thickness
%   +--------- q2,q3 ->@-------------------+
%   |                  |                   | 
%   |                  :                   | w
%   |                  |                   | i
%   |                  :                   | d
%   |                  |                   | t
%   |                  :                   | h
%   |                  |                   | 
%   +------------------:-------------------+
%                      |     
%                      D (x3)
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% q2, q3             east and depth coordinates of the shear zone,
% T, W               thickness and width of the shear zone,
% epsvij             source strain component 22, 23 and 33 in the shear zone
%                    in the system of reference tied to the shear zone,
% G, nu              shear modulus and Poisson's ratio in the half space.
%
% Output:
% u2                 displacement component in the east direction,
% u3                 displacement component in the down direction.
%

% Lame parameter
lambda=G*2*nu/(1-2*nu);

% isotropic strain
epsvkk=epsv22+epsv33;

% Green's functions
r12=@(y2,y3) (x2-y2).^2+(x3-y3).^2;
r22=@(y2,y3) (x2-y2).^2+(x3+y3).^2;

I223=@(y2,y3) ...
    (1/2*(4*nu-3)*y3-2*nu*x3).*log(r12(y2,y3)) + (2*nu*(3-2*nu)*(x3+y3)-5/2*y3).*log(r22(y2,y3)) ...
    +(x2-y2).*( (4*nu-3)*atan2((x2-y2),(x3-y3))+(4*nu*(2*nu-3)+5)*atan2((x2-y2),(x3+y3)) ...
               +(3-4*nu)*atan2((x3+y3),(x2-y2))-atan((x3-y3)./(x2-y2))) ...
    +(2*x3.*((x2-y2).^2+x3.*(x3+y3)))./(r22(y2,y3))-3*x3.*atanh((2*x3*y3)./((x2-y2).^2+x3.^2+y3.^2));

I222=@(y2,y3) ...
    0.5*(x2-y2).*((3-4*nu).*log(r12(y2,y3))+(4*nu*(2*nu-3)+5)*log(r22(y2,y3))-(4*x3.*y3)./(r22(y2,y3))) ...
    +(x3-y3).*((4*nu-3)*atan2((x3-y3),(x2-y2))+atan((x2-y2)./(x3-y3))) ...
    -(x3+y3).*((4*nu-3)*atan((x2-y2)./(x3+y3))+(4*nu*(2*nu-3)+5)*atan2((x3+y3),(x2-y2)));

I323=@(y2,y3) ...
    0.5.*(x2-y2).*(-log(r12(y2,y3))-(8*nu*(nu-2)+7)*log(r22(y2,y3))-(4*x3.*y3)./(r22(y2,y3)))...    
    +4*(nu-1)*((2*nu-3)*x3.*atan((x3+y3)./(x2-y2))+(1-2*nu)*y3*atan((x2-y2)./(x3+y3)));

I233=@(y2,y3) ...
    (x2-y2).*(4*(nu-1)*nu*log(r22(y2,y3))+(2*x3*y3)./(r22(y2,y3))+atanh((2*x3*y3)./((x2-y2).^2+x3.^2+y3^2))) ...
    +4*(1-2*nu)*(nu*x3.*atan((x3+y3)./(x2-y2))+(1-nu)*y3*atan((x2-y2)./(x3+y3)));

I232=@(y2,y3) ...
    0.5*((8*(nu-1)*nu+1)*x3+(8*(nu-2)*nu+7)*y3).*log(r22(y2,y3))-0.5*(x3-y3).*log(r12(y2,y3)) ...
    +4*(1-nu)*(2*nu-1)*(x2-y2).*atan((x2-y2)./(x3+y3))-(2*y3*x3.*(x3+y3))./(r22(y2,y3));

I333=@(y2,y3) ...
    (0.5*(4*nu-3)*y3-2*nu*x3).*log(r12(y2,y3))+(2*nu*(3-2*nu)*(x3+y3)-5*y3/2).*log(r22(y2,y3)) ...
    +(x2-y2).*( (4*nu-3)*atan2((x2-y2),(x3-y3))+(4*nu*(2*nu-3)+5)*atan2((x2-y2),(x3+y3)) ...
               -(3-4*nu)*atan((x3+y3)./(x2-y2))+atan((x3-y3)./(x2-y2))) ...
    +(2*x3.*((x2-y2).^2+x3.*(x3+y3)))./(r22(y2,y3))-3*x3.*atanh((2*y3*x3)./((x2-y2).^2+x3.^2+y3^2));

I322=@(y2,y3) ...
    0.5*((8*(1-nu)*nu-1)*y3-(8*(nu-2)*nu+7)*x3).*log(r22(y2,y3))-0.5*(x3-y3).*log(r12(y2,y3)) ...
    +4*(nu-1)*(2*nu-1)*(x2-y2).*atan((x2-y2)./(x3+y3))+(2*y3*x3.*(x3+y3))./(r22(y2,y3));

I332=@(y2,y3) ...
    0.5*(x2-y2).*((4*nu*(2*nu-3)+5)*log(r22(y2,y3))-(4*nu-3)*log(r12(y2,y3))-(4*y3*x3)./(r22(y2,y3))) ...
    +(x3-y3).*((4*nu-3)*atan2((x3-y3),(x2-y2))-atan((x2-y2)./(x3-y3))) ...
    +(x3+y3).*((4*nu-3)*atan((x2-y2)./(x3+y3))-(4*nu*(2*nu-3)+5)*atan2((x3+y3),(x2-y2)));


IU2=@(y2,y3) ...
    1/(8*pi*G*(1-nu))*((lambda*epsvkk+2*G*epsv22)*I223(y2,y3)+2*G*epsv23*(I222(y2,y3)+I323(y2,y3))+(lambda*epsvkk+2*G*epsv33)*I322(y2,y3));

IU3=@(y2,y3) ...
     1/(8*pi*G*(1-nu))*((lambda*epsvkk+2*G*epsv22)*I233(y2,y3)+2*G*epsv23*(I232(y2,y3)+I333(y2,y3))+(lambda*epsvkk+2*G*epsv33)*I332(y2,y3));

u2=IU2(q2+T/2,q3+W)-IU2(q2-T/2,q3+W)+IU2(q2-T/2,q3)-IU2(q2+T/2,q3);
u3=IU3(q2+T/2,q3+W)-IU3(q2-T/2,q3+W)+IU3(q2-T/2,q3)-IU3(q2+T/2,q3);

end








